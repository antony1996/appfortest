package com.example.appfortest.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.example.appfortest.R;
import com.example.appfortest.Service.MyIntentService;

public class HtmlActivity extends AppCompatActivity {

    WebView webView;
    String urll;
    boolean block;

    SharedPreferences sharedPreferences;

    ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_html);

        urll = getIntent().getStringExtra("url");
        block = getIntent().getBooleanExtra("block", true);

            sharedPreferences = getSharedPreferences("carsData", Context.MODE_PRIVATE);

        if(urll == null && block) {
            urll = sharedPreferences.getString("url", "");
        }

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(urll);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.setWebViewClient(new MyWebViewClient());

    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            urll = url;
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        if(!block) {
            if (webView.canGoBack()) {
                webView.goBack();
            } else {
                super.onBackPressed();
            }
        }else{
            System.out.println("");
        }
    }
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            if(block) {
                System.out.println("");
                return true;
            }else {
                return  false;
            }
        }
        return super.onKeyUp(keyCode, event);
    }


    @Override
    protected void onUserLeaveHint() {
        if(block) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("url", urll);
            editor.apply();
            Intent intentMyIntentService = new Intent(getApplicationContext(), MyIntentService.class);
            startService(intentMyIntentService);
        }
    }



}
