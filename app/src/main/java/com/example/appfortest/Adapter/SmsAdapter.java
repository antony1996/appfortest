package com.example.appfortest.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.appfortest.Model.Sms;
import com.example.appfortest.R;

import java.util.List;

/**
 * Created by Антон on 03.02.2017.
 */

public class SmsAdapter extends ArrayAdapter<Sms> {

    Context context;
    List<Sms> smsArray;

    public SmsAdapter(Context context, int resource, List<Sms> smsArray) {
        super(context, resource, smsArray);
        this.context = context;
        this.smsArray = smsArray;
    }

    static class ViewHolder {
        TextView number;
        TextView text;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_sms, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.number = (TextView) convertView.findViewById(R.id.smsNumber);
            viewHolder.text = (TextView) convertView.findViewById(R.id.smsText);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.number.setText(smsArray.get(position).getAddress());
        viewHolder.text.setText(smsArray.get(position).getMsg());
        return convertView;
    }
}
