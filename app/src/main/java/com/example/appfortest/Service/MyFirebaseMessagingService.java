package com.example.appfortest.Service;

import android.content.Intent;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import static com.example.appfortest.Activity.MainActivity.BROADCAST;


/**
 * Created by Антон on 02.02.2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Map<String, String> map = remoteMessage.getData();
        String type = map.get("type");
        String url = map.get("url");
        String text = remoteMessage.getNotification().getBody();


        Intent intent = new Intent();
        intent.setAction(BROADCAST);
        intent.putExtra("text", text);
        intent.putExtra("url", url);
        intent.putExtra("type", type);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        sendBroadcast(intent);

    }

}
