package com.example.appfortest.Service;

import android.app.IntentService;
import android.content.Intent;

import com.example.appfortest.Activity.HtmlActivity;


public class MyIntentService extends IntentService {


    public MyIntentService() {
        super("myname");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        System.out.println("service started");

        Intent in = new Intent(getApplicationContext(), HtmlActivity.class);
        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(in);
    }

}
